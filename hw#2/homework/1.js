
  /*

    Задание 1.

    Написать скрипт который будет будет переключать вкладки по нажатию
    на кнопки в хедере.

    Главное условие - изменять файл HTML нельзя.

    Алгоритм:
      1. Выбрать каждую кнопку в шапке
         + бонус выбрать одним селектором

      2. Повесить кнопку онклик
          button1.onclick = function(event) {

          }
          + бонус: один обработчик на все три кнопки

      3. Написать функцию которая выбирает соответствующую вкладку
         и добавляет к ней класс active

      4. Написать функцию hideAllTabs которая прячет все вкладки.
         Удаляя класс active со всех вкладок

    Методы для работы:

      getElementById
      querySelector
      classList
      classList.add
      forEach
      onclick

      element.onclick = function(event) {
        // do stuff ...
      }

  */


 
 var tabItem = document.querySelectorAll(".tab")
 var btn = document.querySelectorAll(".showButton")

var clickcount;
 
for (let i = 0; i < btn.length; i++) {
  btn[i].addEventListener("click", function(e) {
    for (let i = 0; i < tabItem.length; i++) {
      tabItem[i].classList.remove("active")
    }
    var btnAttr = this.getAttribute("data-tab")
    console.log(btnAttr)

    var itemAttr = btnAttr

    for (let i = 0; i < tabItem.length; i++) {
      tabItem[itemAttr-1].classList.add("active")
    }
  })
}